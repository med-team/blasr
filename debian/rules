#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

export VERBOSE=1 # for cmake
export SH_LIB_EXT=.so

export HDF5_INC=/usr/include/hdf5/serial
export HDF5_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial
export BOOST_INC=/usr/include/boost
export HTSLIB_INC=/usr/include/htslib
export HTSLIB_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)
export PBBAM_INC=/usr/include
export PBBAM_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)
export PBDATA_ROOT_DIR=/usr/include/pbseq
export LIBBLASR_INC=$(PBDATA_ROOT_DIR)/alignment
export LIBBLASR_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)
export LIBPBDATA_INC=$(PBDATA_ROOT_DIR)/pbdata
export LIBPBDATA_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)
export LIBPBIHDF_INC=$(PBDATA_ROOT_DIR)/hdf
export LIBPBIHDF_LIB=/usr/lib/$(DEB_HOST_MULTIARCH)

# Needed for HDF5 1.10.1 support
HDF5_VERSION := $(shell grep 'HDF5 Version' $(HDF5_LIB)/libhdf5.settings | awk '{print $$3}')
ifeq (yes,$(shell dpkg --compare-versions $(HDF5_VERSION) '>=' 1.10.1 && echo yes))
export DEB_CXXFLAGS_MAINT_APPEND=-DHAVE_HDF5_1_10_1
endif

# CPPFLAGS apparently aren't exported for bam2bax/bax2bam
export DEB_CXXFLAGS_MAINT_PREPEND = $(CPPFLAGS) -I$(PBDATA_ROOT_DIR)

%:
	dh $@ --buildsystem=meson

override_dh_auto_configure:
	LDFLAGS="-L$(HTSLIB_LIB) -L$(HDF5_LIB) -lhdf5_cpp -lhdf5 $(LDFLAGS)" CPPFLAGS="-isystem $(HDF5_INC)" dh_auto_configure -- -Dtests=false

bax2bam: utils/bax2bax/bin/bax2bam
utils/bax2bax/bin/bax2bam:
	$(MAKE) -C utils/bax2bam

bam2bax: utils/bam2bax/bin/bam2bax
utils/bam2bax/bin/bam2bax:
	$(MAKE) -C utils/bam2bax

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	echo Tests require data not available in the source distribution
endif
